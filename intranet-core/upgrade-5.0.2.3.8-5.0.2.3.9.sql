-- upgrade-5.0.2.3.8-5.0.2.3.9.sql
SELECT acs_log__debug('/packages/intranet-core/sql/postgresql/upgrade/upgrade-5.0.2.3.8-5.0.2.3.9.sql','');

DO $$
BEGIN
    -- Check if the column 'project_wbs' already exists in 'im_projects'
    IF NOT EXISTS (
        SELECT 1 
        FROM information_schema.columns 
        WHERE table_name='im_projects' 
        AND column_name='project_wbs'
    ) THEN
        ALTER TABLE im_projects ADD COLUMN project_wbs TEXT;
    END IF;
END $$;
