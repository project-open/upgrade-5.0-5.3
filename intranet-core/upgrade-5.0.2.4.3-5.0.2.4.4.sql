-- upgrade-5.0.2.4.3-5.0.2.4.4.sql
SELECT acs_log__debug('/packages/intranet-core/sql/postgresql/upgrade/upgrade-5.0.2.4.3-5.0.2.4.4.sql','');

-- set the iso codes to lower case
SET session_replication_role = 'replica';

update country_codes 
set country_name = 'Taiwan (ROC)' 
where country_name = 'Taiwan';

-- Ensure unique constraint handling
DO $$
DECLARE
    rec RECORD;
BEGIN
    FOR rec IN SELECT iso FROM country_codes LOOP
        BEGIN
            UPDATE country_codes SET iso = lower(rec.iso) WHERE iso = rec.iso;
        EXCEPTION WHEN unique_violation THEN
            -- Handle duplicate case
            UPDATE country_codes SET iso = lower(rec.iso) || '_dup' WHERE iso = rec.iso;
        END;
    END LOOP;
END $$;

update im_offices set address_country_code = lower(address_country_code);

SET session_replication_role = 'origin';
