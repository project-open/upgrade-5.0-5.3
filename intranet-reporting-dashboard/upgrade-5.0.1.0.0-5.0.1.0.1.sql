SELECT acs_log__debug('/packages/intranet-reporting-dashboard/sql/postgresql/upgrade/upgrade-5.0.1.0.0-5.0.1.0.1.sql','');


SELECT im_component_plugin__new (
	null, 'im_component_plugin', now(), null, null, null,	-- system params
	'Top Customers',					-- plugin_name
	'intranet-reporting-dashboard',				-- package_name
	'left',							-- location
	'/intranet/index',					-- page_url
	null,							-- view_name
	100,							-- sort_order
	'im_dashboard_top_customers -diagram_width 580 -diagram_height 300 -diagram_max_customers 8',
	'lang::message::lookup "" intranet-reporting-dashboard.Top_Customers "Top Customers"'
);
SELECT acs_permission__grant_permission(
	(select plugin_id from im_component_plugins where plugin_name = 'Top Customers' and page_url = '/intranet/index'),
	(select group_id from groups where group_name = 'Employees'), 
	'read'
);


SELECT im_component_plugin__new (
	null, 'im_component_plugin', now(), null, null, null,	-- system params
	'Top Customers (Company Dashboard)',			-- plugin_name
	'intranet-reporting-dashboard',				-- package_name
	'left',							-- location
	'/intranet/companies/dashboard',			-- page_url
	null,							-- view_name
	10,							-- sort_order
	'im_dashboard_top_customers -diagram_width 580 -diagram_height 300 -diagram_max_customers 8',
	'lang::message::lookup "" intranet-reporting-dashboard.Top_Customers "Top Customers"'
);
SELECT acs_permission__grant_permission(
	(select plugin_id from im_component_plugins 
	 where  plugin_name = 'Top Customers (Company Dashboard)' and 
	        page_url = '/intranet/companies/dashboard'),
	(select group_id from groups where group_name = 'Employees'), 
	'read'
);

SELECT im_component_plugin__new (
	null, 'im_component_plugin', now(), null, null, null,	-- system params
	'Top Customers (Finance Dashboard)',			-- plugin_name
	'intranet-reporting-dashboard',				-- package_name
	'left',							-- location
	'/intranet-invoices/dashboard',				-- page_url
	null,							-- view_name
	10,							-- sort_order
	'im_dashboard_top_customers -diagram_width 580 -diagram_height 300 -diagram_max_customers 8',
	'lang::message::lookup "" intranet-reporting-dashboard.Top_Customers "Top Customers"'
);
SELECT acs_permission__grant_permission(
	(select plugin_id from im_component_plugins 
	 where plugin_name = 'Top Customers (Finance Dashboard)' and 
		page_url = '/intranet-invoices/dashboard'
	),
	(select group_id from groups where group_name = 'Employees'), 
	'read'
);



SELECT im_component_plugin__new (
	null, 'im_component_plugin', now(), null, null, null,
	'Pre-Sales Queue',			-- plugin_name
	'intranet-reporting-dashboard',		-- package_name
	'left',				-- location
	'/intranet/projects/dashboard',		-- page_url
	null,					-- view_name
	100,					-- sort_order
	'im_dashboard_histogram_sql -diagram_width 400 -sql "
		select	im_lang_lookup_category(''[ad_conn locale]'', p.project_status_id) as project_status,
		        sum(coalesce(presales_probability,project_budget,0) * coalesce(presales_value,0)) as value
		from	im_projects p
		where	p.project_status_id not in (select * from im_sub_categories(81))
		group by project_status_id
		order by project_status
	"',
	'lang::message::lookup "" intranet-reporting-dashboard.Sales_Pipeline "Sales<br>Pipeline"'
);
SELECT acs_permission__grant_permission(
	(select plugin_id from im_component_plugins 
	 where plugin_name = 'Pre-Sales Queue' and 
		page_url = '/intranet/projects/dashboard'
	),
	(select group_id from groups where group_name = 'Employees'), 
	'read'
);



-- Absences per department
--
SELECT im_component_plugin__new (
	null, 'im_component_plugin', now(), null, null, null,
	'Users per Department',		    	-- plugin_name
	'intranet-reporting-dashboard',		-- package_name
	'left',					-- location
	'/intranet/users/dashboard',		-- page_url
	null,					-- view_name
	10,					-- sort_order
	'im_dashboard_histogram_sql -diagram_width 400 -sql "
	select	im_cost_center_code_from_id(cost_center_id) || '' - '' || im_cost_center_name_from_id(cost_center_id),
		round(coalesce(user_sum, 0.0), 1)
	from	(
		select	cost_center_id,
			tree_sortkey,
			(select count(*) from im_employees e where e.department_id = cc.cost_center_id) as user_sum
		from	im_cost_centers cc
		where	1 = 1
		) t
	where	user_sum > 0
	order by tree_sortkey
	"',
	'lang::message::lookup "" intranet-reporting-dashboard.Users_per_department "Users per Department"'
);
SELECT acs_permission__grant_permission(
	(select plugin_id from im_component_plugins 
	 where plugin_name = 'Users per Department'),
	(select group_id from groups where group_name = 'Employees'), 
	'read'
);






-- Absences per department
--
SELECT im_component_plugin__new (
	null, 'im_component_plugin', now(), null, null, null,
	'Average Absences Days per User',		-- plugin_name
	'intranet-reporting-dashboard',		-- package_name
	'left',					-- location
	'/intranet-timesheet2/absences/dashboard',	-- page_url
	null,					-- view_name
	10,					-- sort_order
	'im_dashboard_histogram_sql -diagram_width 400 -sql "
	select	im_cost_center_code_from_id(cost_center_id) || '' - '' || im_cost_center_name_from_id(cost_center_id),
		round(coalesce(1.0 * absence_sum / user_sum, 0.0), 1)
	from	(
		select	cost_center_id,
			tree_sortkey,
			(select count(*) from im_employees e where e.department_id = cc.cost_center_id
			) as user_sum,
			(select	sum(ua.duration_days)
			 from	im_user_absences ua,
			 	im_employees e
			 where	e.department_id = cc.cost_center_id and
			 	e.employee_id = ua.owner_id and
				ua.end_date > now()::date - 365
			) as absence_sum
		from	im_cost_centers cc
		where	1 = 1
		) t
	where	user_sum > 0
	order by tree_sortkey
	"',
	'lang::message::lookup "" intranet-reporting-dashboard.Average_absence_days_per_user_and_department "Average Absences Days per User"'
);
SELECT acs_permission__grant_permission(
	(select plugin_id from im_component_plugins 
	 where plugin_name = 'Average Absences Days per User'),
	(select group_id from groups where group_name = 'Employees'), 
	'read'
);

