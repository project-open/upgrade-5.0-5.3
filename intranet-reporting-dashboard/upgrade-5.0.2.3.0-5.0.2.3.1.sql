-- intranet-reporting-dashboard/sql/postgresql/upgrade/upgrade/upgrade-5.0.2.3.0-5.0.2.3.1.sql

SELECT acs_log__debug('/packages/intranet-reporting-dashboard/sql/postgresql/upgrade/upgrade-5.0.2.3.0-5.0.2.3.1.sql','');

-- Delete the "all time top services" portlet
select im_component_plugin__delete(
       (select min(plugin_id) 
       from im_component_plugins 
       where plugin_name = 'Home All-Time Top Services')
);

update im_component_plugins
set component_tcl = 
	'im_dashboard_histogram_sql -diagram_width 400 -sql "
		select	im_lang_lookup_category(''[ad_conn locale]'', p.project_status_id) as project_status,
		        sum(coalesce(presales_probability,project_budget,0) * coalesce(presales_value,0)) as value
		from	im_projects p
		where	p.parent_id is null and
			p.project_status_id not in (select * from im_sub_categories(81))
		group by project_status_id
		order by project_status
	"'
where package_name = 'intranet-reporting-dashboard' and plugin_name = 'Pre-Sales Queue';



update im_component_plugins
set component_tcl = 
	'im_dashboard_histogram_sql -diagram_width 400 -sql "
		select	im_lang_lookup_category(''[ad_conn locale]'', p.project_status_id) as project_status,
		        count(*) as cnt
		from	im_projects p
		where	p.parent_id is null and
			p.project_status_id not in (select * from im_sub_categories(81))
		group by project_status_id
		order by project_status
	"'
where package_name = 'intranet-reporting-dashboard' and plugin_name = 'Projects by Status';


